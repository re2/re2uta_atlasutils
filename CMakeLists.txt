cmake_minimum_required(VERSION 2.4.6)
include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
set(ROS_BUILD_TYPE RelWithDebInfo)
#set(ROS_BUILD_TYPE Debug)

rosbuild_init()

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

#add_definitions( -DAUTOLEVROBOTDYNAMICS_FAKE )
rosbuild_add_library( ${PROJECT_NAME} src/AtlasDebugPublisher.cpp
                                      src/AtlasLookup.cpp
                                      src/atlas_msg_utils.cpp )


rosbuild_add_executable( AtlasDebugPublisherTest_node  src/tests/AtlasDebugPublisherTest_node.cpp )
rosbuild_add_executable( AtlasLookupTest_node          src/tests/AtlasLookupTest_node.cpp )

target_link_libraries( AtlasDebugPublisherTest_node ${PROJECT_NAME} )
target_link_libraries( AtlasLookupTest_node         ${PROJECT_NAME} )


