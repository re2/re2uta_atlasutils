/*
 * AtlasLookup.hpp
 *
 *  Created on: Mar 19, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <kdl/tree.hpp>
#include <urdf/model.h>
#include <re2/kdltools/kdl_tools.h>
#include <re2/kdltools/kdl_tree_util.hpp>

#include <atlas_msgs/AtlasCommand.h>
#include <atlas_msgs/AtlasState.h>

#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>

#include <string>
#include <vector>
#include <map>

#include <ros/ros.h>

namespace re2uta
{


class AtlasLookup
{
    public:
        typedef boost::shared_ptr<AtlasLookup>                  Ptr;

        typedef std::map<std::string, int>                      StringToIndexMap;
        typedef std::map<std::string, const KDL::TreeElement *> JointNameToTreeElementMap;
        typedef std::vector<const KDL::TreeElement*>            TreeElementPtrVector;

    public:
        AtlasLookup( const urdf::Model & model );

        boost::shared_ptr<const KDL::Tree> getTree();
        int                                getNumTreeJoints();
        int                                getNumCmdJoints();

        atlas_msgs::AtlasCommand::Ptr      createEmptyJointCmdMsg();
        atlas_msgs::AtlasCommand::Ptr      createDefaultJointCmdMsg( ros::NodeHandle node );


        Eigen::VectorXd                    calcDefaultTreeJointAngles( double legBend );
        Eigen::VectorXd                    calcDefaultCmdJointAngles(  double legBend );


        std::string                        lookupString( std::string const & query );

//        const std::string &                getLeftFootSegName();
//        const std::string &                getRightFootSegName();
//        const std::string &                getLeftArmSegName();
//        const std::string &                getRightArmSegName();
//        const std::string &                getTorsoSegName();


        const std::vector<std::string> &   getJointPrefixedNameVector();
        const std::vector<std::string> &   getJointNameVector();
        const StringToIndexMap &           getJointNameToIndexMap();
        const StringToIndexMap &           getJointPrefixedNameToIndexMap( const std::vector<std::string> & nameVector );
        const Eigen::VectorXd &            getTreeJointMinVector();
        const Eigen::VectorXd &            getTreeJointMaxVector();
        const Eigen::VectorXd &            getCmdJointMinVector();
        const Eigen::VectorXd &            getCmdJointMaxVector();
        const std::vector<int> &           getTreeQnrToJointIndexVector();
        const std::vector<int> &           getJointToTreeQnrIndexVector();
        const std::vector<const KDL::TreeElement*> & getQnrToTreeElementVector();
        const KDL::TreeElement *           lookupTreeElementFromJointName( const std::string & jointName );
        int                                jointIndexToQnr(         int jointIndex );
        const std::string &                jointIndexToJointName(   int jointIndex );
        const std::string &                jointIndexToSegmentName( int jointIndex );
        int                                jointNameToJointIndex(   const std::string & jointName  );
        int                                jointNameToQnr(          const std::string & jointName  );
        int                                segmentNameToQnr(        const std::string & segmentName );
        int                                segmentNameToJointIndex( const std::string & segmentName );
        int                                treeQnrToJointIndex(     int qnr );
        const std::string &                treeQnrToSegmentName(    int qnr );
        const std::string &                treeQnrToJointName(      int qnr );
        Eigen::VectorXd                    treeToCmd( const Eigen::VectorXd & treeJointValues );
        Eigen::VectorXd                    cmdToTree( const Eigen::VectorXd & cmdJointValues  );


        void buildKdlTreeBasedMaps();


    private:
        static const std::string          emptyString;

//        urdf::Model                        m_urdfModel;
        std::map<std::string,std::string>  m_translationMap;
        boost::shared_ptr<KDL::Tree>       m_tree;
        re2::kdltools::JointNameToAngleMap m_jointMinMap;
        re2::kdltools::JointNameToAngleMap m_jointMaxMap;
        Eigen::VectorXd                    m_treeJointMins;
        Eigen::VectorXd                    m_treeJointMaxs;
        Eigen::VectorXd                    m_cmdJointMins;
        Eigen::VectorXd                    m_cmdJointMaxs;
        StringToIndexMap                   m_jointNameToIndexMap;
        StringToIndexMap                   m_jointPrefixedNameToIndexMap;
        JointNameToTreeElementMap          m_jointNameToTreeElementMap;
        std::vector<std::string>           m_jointIndexToNameVector;
        std::vector<std::string>           m_jointIndexToPrefixedNameVector;
        TreeElementPtrVector               m_qnrToTreeElementVector;
        std::vector<int>                   m_treeQnrToJointIndexVector;
        std::vector<int>                   m_treeJointIndexToQnrVector;
};


template <typename MsgT>
Eigen::VectorXd
parseJointPositions( const MsgT & lastStateMsg )
{
    Eigen::VectorXd jointPositions( lastStateMsg->position.size() );
    int index = 0;
    BOOST_FOREACH( double position, lastStateMsg->position )
    {
        jointPositions( index ) = position;
        ++index;
    }

    return jointPositions;
}

template <typename MsgT>
Eigen::VectorXd
parseJointVelocities( const MsgT & lastStateMsg )
{
    Eigen::VectorXd jointVelocities( lastStateMsg->position.size() );
    int index = 0;
    BOOST_FOREACH( double velocity, lastStateMsg->velocity )
    {
        jointVelocities( index ) = velocity;
        ++index;
    }

    return jointVelocities;
}



} // end namespace

