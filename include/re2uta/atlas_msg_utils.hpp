/*
 * atlas_msg_utils.hpp
 *
 *  Created on: Apr 16, 2013
 *      Author: andrew.somerville
 */

#ifndef ATLAS_MSG_UTILS_HPP_
#define ATLAS_MSG_UTILS_HPP_

#include <osrf_msgs/JointCommands.h>

#include <string>
#include <vector>

osrf_msgs::JointCommandsPtr buildDefaultZeroedJointsCommand( const std::vector<std::string> & nameList );


#endif /* ATLAS_MSG_UTILS_HPP_ */
