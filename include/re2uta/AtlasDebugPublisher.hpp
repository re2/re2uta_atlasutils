/*
 * AtlasDebugPublisher.hpp
 *
 *  Created on: Mar 28, 2013
 *      Author: andrew.somerville
 */

#ifndef ATLASDEBUGPUBLISHER_HPP_
#define ATLASDEBUGPUBLISHER_HPP_

#include <re2uta/AtlasLookup.hpp>

#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>
#include <robot_state_publisher/robot_state_publisher.h>
#include <ros/ros.h>

#include <kdl/tree.hpp>
#include <kdl/treefksolverpos_recursive.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>

#include <string>
#include <vector>
#include <map>


namespace re2uta
{

class AtlasDebugPublisher
{
    public:
        typedef boost::shared_ptr<AtlasDebugPublisher> Ptr;
        typedef boost::shared_ptr<KDL::TreeFkSolverPos_recursive>  KdlTreeFkSolverPos_recursivePtr;


    public:
        AtlasDebugPublisher( const urdf::Model & model, const std::string & ghostPrefix = std::string( "/atlas_cmd" ) );

//        void setTreeRootToBaseTransform( const std::string baseName, const Eigen::Affine3d & treeRootToBaseXform );
        void setBase( const std::string baseName );
        void setWorld( const std::string worldFrame );
        void publishQnrOrderedAngles( const Eigen::VectorXd & qnrJointAngles );
        void publishCmdOrderedAngles( const Eigen::VectorXd & jointCmdAngles );

//        void publishVectorsAtJoints( const Eigen::VectorXd & qnrJointAngles );

        AtlasLookup::Ptr lookup();

    private:
        void publishJointPositionMap();

    private:
        typedef std::map<std::string, double>                                 JointPositionMap;
        typedef boost::shared_ptr<robot_state_publisher::RobotStatePublisher> RobotStatePublisherPtr;

    private:
        boost::shared_ptr<const KDL::Tree>  m_tree;
        KdlTreeFkSolverPos_recursivePtr     m_fkSolver;
        AtlasLookup::Ptr         m_atlasLookup;
        RobotStatePublisherPtr   m_robotStatePublisher;
        tf::TransformBroadcaster m_tfBroadcaster;
        JointPositionMap         m_jointPositionMap;
        Eigen::VectorXd          m_qnrIndexedjointAngles;
        std::string              m_ghostPrefix;
        std::string              m_baseName;
        std::string              m_worldName;
        std::string              m_rootName;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

} // end namespace


#endif /* ATLASDEBUGPUBLISHER_HPP_ */
