/*
 * msg_utils.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/atlas_msg_utils.hpp>

#include <osrf_msgs/JointCommands.h>

#include <boost/foreach.hpp>

#include <string>
#include <vector>


osrf_msgs::JointCommandsPtr buildDefaultZeroedJointsCommand( const std::vector<std::string> & nameList )
{
    osrf_msgs::JointCommandsPtr message( new osrf_msgs::JointCommands );

    message->header.stamp    = ros::Time::now();
    message->header.frame_id = "";

    message->name = nameList;
    message->effort      .resize( message->name.size() );
    message->position    .resize( message->name.size() );
    message->velocity    .resize( message->name.size() );
    message->kp_position .resize( message->name.size() );
    message->kd_position .resize( message->name.size() );
    message->kp_velocity .resize( message->name.size() );
    message->i_effort_max.resize( message->name.size() );
    message->i_effort_min.resize( message->name.size() );

    int cmdJointIndex = 0;
    BOOST_FOREACH( const std::string & name, message->name )
    {
        message->effort[       cmdJointIndex ] = 0;
        message->position[     cmdJointIndex ] = 0;
        message->velocity[     cmdJointIndex ] = 0;
        message->kp_position[  cmdJointIndex ] = 0;
        message->kd_position[  cmdJointIndex ] = 0;
        message->kp_velocity[  cmdJointIndex ] = 0;
        message->i_effort_max[ cmdJointIndex ] = 0;
        message->i_effort_min[ cmdJointIndex ] = 0;
        ++ cmdJointIndex;
    }

    return message;
}
