#include <re2uta/AtlasDebugPublisher.hpp>

#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_kdl.h>


namespace re2uta
{

AtlasDebugPublisher::
AtlasDebugPublisher( const urdf::Model & model, const std::string & ghostPrefix )
{
    m_atlasLookup.reset( new AtlasLookup( model ) );
    m_tree        = m_atlasLookup->getTree();
    m_fkSolver.reset( new KDL::TreeFkSolverPos_recursive( *m_tree ) );

    m_ghostPrefix = ghostPrefix;

    m_rootName    = "root";
    m_baseName    = "l_foot";
    ros::NodeHandle privateNh( "~" );
    privateNh.setParam( "tf_prefix", m_ghostPrefix );
    m_robotStatePublisher.reset( new robot_state_publisher::RobotStatePublisher(*m_tree) );
    privateNh.setParam( "tf_prefix", "" );


    if(    !m_ghostPrefix.empty()
        && m_ghostPrefix.find_last_of("/") != m_ghostPrefix.size() - 1 )
    {
        m_ghostPrefix += "/";
    }

    if( m_ghostPrefix.size() == 1 && m_ghostPrefix[0] == '/' )
        m_ghostPrefix = "";


//    m_treeRootToBaseXform = Eigen::Affine3d::Identity();
    m_qnrIndexedjointAngles = Eigen::VectorXd::Zero(m_tree->getNrOfJoints());
}


//void
//AtlasDebugPublisher::
//setTreeRootToBaseTransform( const std::string baseName, const Eigen::Affine3d & treeRootToBaseXform )
//{
//    m_baseName            = baseName;
//}

void
AtlasDebugPublisher::
setBase( const std::string baseName )
{
    m_baseName = baseName;
}

void
AtlasDebugPublisher::
setWorld( const std::string worldName )
{
    m_worldName = worldName;

    if( !m_worldName.empty() && m_worldName[0] != '/' )
        m_worldName = std::string( "/" ) + m_worldName;
}

AtlasLookup::Ptr
AtlasDebugPublisher::
lookup()
{
    return m_atlasLookup;
}


void
AtlasDebugPublisher::
publishQnrOrderedAngles( const Eigen::VectorXd & qnrJointAngles )
{
    m_qnrIndexedjointAngles = qnrJointAngles;
    for( int qnr = 0; qnr < qnrJointAngles.rows(); ++qnr )
    {
        std::string name = m_atlasLookup->treeQnrToJointName( qnr );
        m_jointPositionMap[name] = qnrJointAngles(qnr);
    }

    publishJointPositionMap();
}

void
AtlasDebugPublisher::
publishCmdOrderedAngles( const Eigen::VectorXd & jointCmdAngles )
{
    publishQnrOrderedAngles( m_atlasLookup->cmdToTree( jointCmdAngles ) );
}


//void
//AtlasDebugPublisher::
//publishVectorsAtJoints( const Eigen::VectorXd & qnrJointAngles )
//{
//    m_qnrIndexedjointAngles = qnrJointAngles;
//    for( int qnr = 0; qnr < qnrJointAngles.rows(); ++qnr )
//    {
//        std::string name = m_atlasLookup->treeQnrToJointName( qnr );
//        m_jointPositionMap[name] = qnrJointAngles(qnr);
//    }
//
//    publishJointPositionMap();
//}

Eigen::Affine3d
getTransform( KDL::TreeFkSolverPos_recursive & treeSolver,
              const std::string & baseFrame,
              const std::string & tipFrame,
              const Eigen::VectorXd & jointPositions )
{
    KDL::Frame baseToPelvis;
    KDL::Frame tipToPelvis;


    KDL::JntArray kdlJointPositions;
    kdlJointPositions.data = jointPositions; //FSCK YOU KDL! What is the point of this damn class!

    int errorVal = 0;
    if( errorVal >= 0 )
        errorVal = treeSolver.JntToCart( kdlJointPositions,  baseToPelvis, baseFrame );
    if( errorVal >= 0 )
        errorVal = treeSolver.JntToCart( kdlJointPositions,  tipToPelvis,  tipFrame  );


    if( errorVal != 0 )
    {
        ROS_WARN_STREAM( "jtnToCart for " << baseFrame << " to " << tipFrame << " gave error code: " << errorVal );
        ROS_WARN_STREAM( "Jointpositions were: " << jointPositions.transpose() );
    }


    KDL::Frame tipToBase = baseToPelvis.Inverse() * tipToPelvis;

    Eigen::Affine3d tipToBaseTransform( Eigen::Affine3d::Identity() );
    tf::transformKDLToEigen( tipToBase, tipToBaseTransform );

    return tipToBaseTransform;
}


void
AtlasDebugPublisher::
publishJointPositionMap()
{
    m_robotStatePublisher->publishFixedTransforms();
    m_robotStatePublisher->publishTransforms( m_jointPositionMap, ros::Time::now() );

    Eigen::Affine3d rootToBaseXform;
    rootToBaseXform = getTransform( *m_fkSolver, m_baseName, m_rootName, m_qnrIndexedjointAngles );


    if( m_worldName.empty() )
        m_worldName = m_baseName;

    tf::Transform treeRootToBase;
    tf::transformEigenToTF( rootToBaseXform, treeRootToBase );
    m_tfBroadcaster.sendTransform( tf::StampedTransform( treeRootToBase,
                                                         ros::Time::now(),
                                                         m_worldName,
                                                         m_ghostPrefix + m_rootName ));
}



}
