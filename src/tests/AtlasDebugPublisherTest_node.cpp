/*
 * AtlasDebugPublisherTest_node.cpp
 *
 *  Created on: Mar 28, 2013
 *      Author: andrew.somerville
 */



#include <re2uta/AtlasDebugPublisher.hpp>

#include <robot_state_publisher/robot_state_publisher.h>

#include <tf/transform_broadcaster.h>

#include <kdl_parser/kdl_parser.hpp>
#include <urdf_model/model.h>

#include <ros/ros.h>

#include <iostream>
#include <fstream>


using namespace re2uta;

class AtlasDebugPublisherTest
{
    private:
        typedef std::map<std::string, double> JointPositionMap;

        ros::NodeHandle  m_node;
        urdf::Model      m_urdfModel;
        KDL::Tree        m_tree;
        Eigen::VectorXd  m_jointPositions;
        Eigen::VectorXd  m_jointMins;
        Eigen::VectorXd  m_jointMaxs;
        JointPositionMap m_jointPositionMap;

    public:
        AtlasDebugPublisherTest()
        {
            std::string simpleUrdfString;

            m_node.getParam( "/robot_description", simpleUrdfString );

            m_urdfModel.initString( simpleUrdfString );

            kdl_parser::treeFromString( simpleUrdfString, m_tree );
        }

        void go()
        {
            AtlasDebugPublisher debugPublisher( m_urdfModel );
//            debugPublisher.setTreeRootToBaseTransform();

            Eigen::VectorXd     qnrOrderedJointAngles;

            qnrOrderedJointAngles = Eigen::VectorXd::Zero( m_tree.getNrOfJoints() );

            while( ros::ok() )
            {
                debugPublisher.publishQnrOrderedAngles( qnrOrderedJointAngles );
                ros::Duration(0.02).sleep();
            }
        }
};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "atlas_debug_publisher_test" );
    ros::NodeHandle node;

    AtlasDebugPublisherTest atlasDebugPublisherTest;

    atlasDebugPublisherTest.go();

    return 0;
}
